package factoryMethod.Button;

public interface Button {
	
	public void click();
	
	public void enable();
	
	
	public void disable();
	
	

}
