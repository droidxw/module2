package abstractFactory.furniture;



public interface Table
{
    void putSomethingOn(String article);
}
