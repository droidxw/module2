package abstractFactory.furniture;


public interface Chair
{
    void sitOn(String person);
}
