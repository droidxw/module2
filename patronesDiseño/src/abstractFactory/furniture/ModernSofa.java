package abstractFactory.furniture;



public class ModernSofa implements Sofa
{
    @Override
    public void layOn(String persons)
    {
        System.out.println(persons + " are sitting on a modern sofa");
    }
}
