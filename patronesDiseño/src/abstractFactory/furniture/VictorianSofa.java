package abstractFactory.furniture;



public class VictorianSofa implements Sofa
{
    @Override
    public void layOn(String persons)
    {
        System.out.println(persons + " are sitting on a victorian sofa");
    }
}
